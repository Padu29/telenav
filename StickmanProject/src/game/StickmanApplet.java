package game;

import java.awt.Graphics;

/**
 * This is the main class, which is the applet itself. It extends the
 * "behind the scenes" class. Methods like "void init" for creating the frame
 * and starting the thread, "void paint" for drawing the objects and
 * "void update" which keeps the graphics updated have been implemented.
 * 
 * @author Andrei Paduraru 30221, 2nd semigroup
 * @email andrei.rapid@yahoo.com
 *
 */
@SuppressWarnings("serial")
public class StickmanApplet extends StickmanLoop {

	/**
	 * This method is used to set the frame of the java application, create the
	 * graphics and add the listeners.
	 */
	
	
	public void init() {
		setSize(854, 480);
		setName("Flying Stickman");
		Thread th = new Thread(this);
		th.start();
		new Thread(new Runnable() {
			public void run() {
				countdown(100);
			}
		}).start();
		offscreen = createImage(854, 480);
		d = offscreen.getGraphics();
		addKeyListener(this);
	}

	/**
	 * This method is used to draw the objects and place them correctly.
	 * 
	 * @param g
	 *            - graphics
	 */
	public void paint(Graphics g) {
		
		d.clearRect(0, 0, 854, 480);
		d.drawImage(background, 0, 0, this);
		d.drawImage(controls, 427, 400, this);
		d.drawString("Score: ", 5, 20);
		d.drawString("Time : ", 5, 40);
		d.drawString(retainscore, 45, 20);
		d.drawString(retaintime, 45, 40);
		//if(retaintime==String.valueOf(1)){
			//d.drawImage(gameover, 0, 0, this);
		//	g.drawImage(offscreen, 0, 0, this);
		//}
		d.drawString("Collect as many bubbles as you can before the time expires!", 5, 400);
		
		d.fillOval(ovalX, ovalY, 20, 20);
		d.drawImage(stickman, x, y, this);
		g.drawImage(offscreen, 0, 0, this);
		
	}

	/**
	 * This method is used to update the graphics.
	 * 
	 * @param g
	 *            - graphics
	 */
	public void update(Graphics g) {
		paint(g);
	}
}
