package game;

import java.applet.Applet;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

/**
 * This class is "behind the scenes" of the application and it uses Apllet,
 * Runnable and KeyListner. It makes the figure move and creates gravity.
 * 
 * @author Andrei Paduraru 30221, 2nd semigroup
 * @email andrei.rapid@yahoo.com
 */
@SuppressWarnings("serial")
public class StickmanLoop extends Applet implements Runnable, KeyListener {

	// protected variables
	protected int x;
	protected int y;
	protected int ovalX, ovalY;
	protected Image offscreen;
	protected Graphics d;
	protected BufferedImage background, stickman, gameover,controls;
	protected String retainscore = "";
	protected String retaintime = "";

	// private variables
	private boolean up, right, left;
	private int incr;
	private double jump = 4;
	private int score = 0;
	private BufferedImage sj, ss, sw1, sw2, sw3;

	/**
	 * Applet's "main" method.
	 * It contains exceptions, coordinates for buffered images. 
	 * This is where gravity, animation, collision is created.
	 */
	public void run() {

		x = 200;
		y = 224;
		randomizeOval();

		try {
			controls = ImageIO.read(new File("controls.png"));
			gameover = ImageIO.read(new File("gameoverbackground.png"));
			background = ImageIO.read(new File("background.png"));
			ss = ImageIO.read(new File("Stickman_standing.png"));
			sj = ImageIO.read(new File("Stickman_jumping.png"));
			sw1 = ImageIO.read(new File("Stickman_walk1.png"));
			sw2 = ImageIO.read(new File("Stickman_walking2.png"));
			sw3 = ImageIO.read(new File("Stickman_walking3.png"));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		stickman = ss;

		while (true) {

			// circles spawning
			if ((Math.abs(x - ovalX) < 27) && (Math.abs(y - ovalY) < 25)) {
				score++;
				retainscore = String.valueOf(score);
				randomizeOval();
				repaint();
			}

			// physics
			if (y <= 150)
				up = false;
			if (y <= 289 && up != true)
				y += 10;
			if (y >= 290)
				y = 290;
			if (x < 0)
				x = 854;
			if (x > 854)
				x = 0;

			// moving animation
			incr++;
			if (incr >= 15) {
				incr = 0;
			}
			if (incr <= 5 && (right == true || left == true)) {
				stickman = sw1;
			}
			if (incr >= 5 && incr <= 10 && (right == true || left == true)) {
				stickman = sw2;
			}
			if (incr <= 10 && incr >= 5 && (right == true || left == true)) {
				stickman = sw3;
			}
			if (incr >= 10 && incr <= 15 && (right == true || left == true)) {
				stickman = ss;
			}
			if (up == true)
				stickman = sj;
			if (up == true && (left == true || right == true))
				stickman = sj;

			// moving controls
			if (left == true)
				x -= 4;
			if (right == true)
				x += 4;
			if (up == true) {
				jump += 0.1;
				y += (int) ((Math.sin(jump) + Math.cos(jump)) * 5);
				if (jump >= 7)
					jump = 4;
			}

			repaint();

			try {
				Thread.sleep(20);

			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * This method is used as a countdown timer.
	 * 
	 * @param nrseconds
	 *            - the number of seconds you want to start from.
	 */
	public void countdown(int nrseconds) {
		int interval = nrseconds;
		long numarafct = 0;
		long starttime = System.currentTimeMillis() / 1000;
		long endtime = starttime + interval;
		while (System.currentTimeMillis() / 1000 < endtime) {
			while (starttime != System.currentTimeMillis() / 1000) {
				starttime++;
				if (endtime - starttime >=0) {
				//	System.out.println(endtime - starttime + " seconds remaining.");
					numarafct = endtime - starttime;
					retaintime = String.valueOf(numarafct);
				}
			}
		}
	}

	/**
	 * This method is used to spawn an oval at random coordinates.
	 */
	public void randomizeOval() {
		ovalX = 10 + (int) (Math.random() * 800);
		ovalY = 205 + (int) (Math.random() * 70);
	}

	/**
	 * Method used to control the figure when key is pressed.
	 */
	public void keyPressed(KeyEvent e) {
		if (e.getKeyCode() == 37)
			left = true;
		if (e.getKeyCode() == 38)
			up = true;
		if (e.getKeyCode() == 39)
			right = true;

	}

	/**
	 * Method used to control the figure when key is released.
	 */
	public void keyReleased(KeyEvent e) {
		if (e.getKeyCode() == 37)
			left = false;
		if (e.getKeyCode() == 38) {
			up = false;
			jump = 4;
		}
		if (e.getKeyCode() == 39)
			right = false;

	}

	/**
	 * Empty method.
	 */
	public void keyTyped(KeyEvent arg0) {

	}
}
